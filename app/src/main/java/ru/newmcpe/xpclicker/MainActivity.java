package ru.newmcpe.xpclicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;

import ru.newmcpe.xpclicker.enums.AchievementType;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener {
    private static final int REQUEST_CODE_SIGN_IN = 2594;
    private GoogleSignInClient googleApiClient;
    private GoogleSignInAccount account;
    private SharedPreferences sharedPreferences;

    private int score;

    private TextView scoreScreen;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.GAMES), new Scope(Scopes.GAMES_LITE))
                .build();

        sharedPreferences = getSharedPreferences("db", Context.MODE_PRIVATE);
        score = getScore();

        scoreScreen = findViewById(R.id.scoreScreen);
        scoreScreen.setText(getString(R.string.allclicks) + score);

        googleApiClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = googleApiClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_CODE_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w("Main", "Error auth in google: " + connectionResult.getErrorMessage());
    }

    @SuppressLint("SetTextI18n")
    public void click(View v) {
        score++;
        scoreScreen.setText(getString(R.string.allclicks) + score);

        checkIfAchievementCanBeUnlocked();
    }

    public void achievementsClick(View v) {
        Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .getAchievementsIntent()
                .addOnSuccessListener(intent -> startActivityForResult(intent, 9003));
    }

    private void checkIfAchievementCanBeUnlocked() {
        for (AchievementType achievementType : Arrays.asList(AchievementType.values())) {
            int clicksAchievement = achievementType.getClicks();
            String achievementId = achievementType.getAchievementId();
            if (clicksAchievement == this.score) {
                Games.getAchievementsClient(this, account)
                        .unlock(achievementId);
                Log.i("achievement", "Unlocked achievement " + achievementType.name());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Toast.makeText(getApplicationContext(), account.getDisplayName(), Toast.LENGTH_LONG).show();
            this.account = account;
            // Signed in successfully, show authenticated UI.
            GamesClient gamesClient = Games.getGamesClient(MainActivity.this, account);
            gamesClient.setViewForPopups(findViewById(R.id.rootLayout));
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("mainclassbass", "signInResult:failed code=" + e.getMessage());
            Toast.makeText(getApplicationContext(), "signInResult:failed code=" + e.getStatusCode(), Toast.LENGTH_LONG).show();

        }
    }

    private void saveScore(int score) {

        sharedPreferences
                .edit()
                .putInt("score", score)
                .apply();
    }

    private int getScore() {
        return sharedPreferences.getInt("score", 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveScore(this.score);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveScore(this.score);
    }
}