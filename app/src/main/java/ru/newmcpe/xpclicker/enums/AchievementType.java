package ru.newmcpe.xpclicker.enums;

import lombok.Getter;

@Getter
public enum AchievementType {

    ACHIEVEMENT_10(10, "CgkIwqaog7weEAIQAA"),
    ACHIEVEMENT_20(20, "CgkIwqaog7weEAIQAw");

    private int clicks;
    private String achievementId;

    AchievementType(int clicks, String achievementId) {
        this.clicks = clicks;
        this.achievementId = achievementId;
    }
}
